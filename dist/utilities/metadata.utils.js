"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_client_1 = require("apollo-client");
const apollo_link_http_1 = require("apollo-link-http");
/* tslint:disable-next-line:no-var-requires */
const fetch = require('node-fetch');
const apollo_cache_inmemory_1 = require("apollo-cache-inmemory");
const info_commons_1 = require("info-commons");
const appeng_meta_1 = __importDefault(require("appeng-meta"));
const getMetaData = async (uri, id, query1, token) => {
    if (info_commons_1.APP_ENV === 'desktop') {
        let query = query1.loc.source.body;
        query = query.trim();
        query = query.substring(query.indexOf("{"));
        query = query.substring(0, query.length);
        query = query.replace("$id", '"' + id + '"');
        const { data } = await appeng_meta_1.default(query);
        return data;
    }
    else {
        const client = createApolloClient(uri);
        const { data } = await client.query({
            query: query1,
            context: {
                headers: {
                    'authorization': token,
                },
            },
            variables: {
                id,
            },
        });
        return data;
    }
};
exports.default = getMetaData;
const createApolloClient = (uri) => {
    const client = new apollo_client_1.ApolloClient({
        ssrMode: true,
        link: new apollo_link_http_1.HttpLink({
            uri,
            fetch,
        }),
        cache: new apollo_cache_inmemory_1.InMemoryCache(),
    });
    return client;
};
//# sourceMappingURL=metadata.utils.js.map