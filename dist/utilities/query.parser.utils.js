"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.queryResolver = (query, params) => {
    for (const key in params) {
        if (params.hasOwnProperty(key) && query.includes(key)) {
            let value = '"' + params[key] + '"';
            // Condition kept for limit clause(if having in query)
            if (typeof params[key] === 'number') {
                value = params[key];
            }
            query = query.replace(new RegExp(':' + key, 'g'), value);
        }
    }
    return query;
};
//# sourceMappingURL=query.parser.utils.js.map