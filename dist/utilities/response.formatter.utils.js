"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const formatResponse = (response) => {
    if (response.data && response.data.columns && response.data.rows) {
        const columns = response.data.columns;
        const record = {};
        response.data.rows.map((row) => {
            for (const [i, value] of row.entries()) {
                record[columns[i]] = value;
            }
        });
        return record;
    }
    return null;
};
exports.default = formatResponse;
//# sourceMappingURL=response.formatter.utils.js.map