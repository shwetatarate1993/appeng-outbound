"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const __1 = require("..");
test('Query resolver with one parameter', async () => {
    const params = { CD_GRAMPANCHAYAT_UUID: '123456789', CD_GRAMPANCHAYAT_ID: '123' };
    const query = 'SELECT CD_GRAMPANCHAYAT_UUID, CD_GRAMPANCHAYAT_ID FROM CONTACT_DAIRY_GRAMPANCHAYAT '
        + 'where CD_GRAMPANCHAYAT_UUID=:CD_GRAMPANCHAYAT_UUID';
    const result = 'SELECT CD_GRAMPANCHAYAT_UUID, CD_GRAMPANCHAYAT_ID FROM CONTACT_DAIRY_GRAMPANCHAYAT '
        + 'where CD_GRAMPANCHAYAT_UUID="123456789"';
    sinon_1.default.assert.match(__1.queryResolver(query, params), result);
});
test('Query resolver with two same parameters', async () => {
    const params = { CD_GRAMPANCHAYAT_UUID: '123456789', CD_GRAMPANCHAYAT_ID: '123' };
    const query = 'SELECT CD_GRAMPANCHAYAT_UUID, CD_GRAMPANCHAYAT_ID FROM CONTACT_DAIRY_GRAMPANCHAYAT '
        + 'where CD_GRAMPANCHAYAT_UUID=:CD_GRAMPANCHAYAT_UUID AND '
        + 'CD_GRAMPANCHAYAT_UUID=:CD_GRAMPANCHAYAT_UUID';
    const result = 'SELECT CD_GRAMPANCHAYAT_UUID, CD_GRAMPANCHAYAT_ID '
        + 'FROM CONTACT_DAIRY_GRAMPANCHAYAT where CD_GRAMPANCHAYAT_UUID="123456789"'
        + ' AND CD_GRAMPANCHAYAT_UUID="123456789"';
    sinon_1.default.assert.match(__1.queryResolver(query, params), result);
});
test('Query resolver with three different parameters', async () => {
    const params = { CD_GRAMPANCHAYAT_UUID: 'abc123', CD_GRAMPANCHAYAT_ID: '123',
        LOGICAL_ENTITY: 'Task' };
    const query = 'SELECT CD_GRAMPANCHAYAT_UUID, CD_GRAMPANCHAYAT_ID FROM CONTACT_DAIRY_GRAMPANCHAYAT '
        + 'where LOGICAL_ENTITY=:LOGICAL_ENTITY AND '
        + 'CD_GRAMPANCHAYAT_UUID=:CD_GRAMPANCHAYAT_UUID AND '
        + 'CD_GRAMPANCHAYAT_ID=:CD_GRAMPANCHAYAT_ID';
    const result = 'SELECT CD_GRAMPANCHAYAT_UUID, CD_GRAMPANCHAYAT_ID FROM CONTACT_DAIRY_GRAMPANCHAYAT '
        + 'where LOGICAL_ENTITY="Task" AND CD_GRAMPANCHAYAT_UUID="abc123" AND '
        + 'CD_GRAMPANCHAYAT_ID="123"';
    sinon_1.default.assert.match(__1.queryResolver(query, params), result);
});
//# sourceMappingURL=query.resolver.test.js.map