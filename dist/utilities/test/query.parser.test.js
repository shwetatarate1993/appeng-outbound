"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const knex_1 = __importDefault(require("knex"));
const __1 = require("..");
test('Query Parser Test', async () => {
    const query = 'SELECT CD_GRAMPANCHAYAT_UUID, CD_GRAMPANCHAYAT_ID '
        + 'FROM CONTACT_DAIRY_GRAMPANCHAYAT where ISDELETED!=1 '
        + '<if test="APP_LOGGED_IN_ROLE_ID==17">	and TS.TALUKA_UUID=:APP_LOGGED_IN_USER_TALUKA </if> '
        + '<if test="APP_LOGGED_IN_ROLE_ID==18">	and TS.TALUKA_UUID=:APP_LOGGED_IN_USER_TALUKA </if>	'
        + '<if test="APP_LOGGED_IN_ROLE_ID==19">	and DS.DISTRICT_UUID=:APP_LOGGED_IN_USER_DISTRICT </if>	'
        + '<if test="APP_LOGGED_IN_ROLE_ID==20">	and CD_GRAMPANCHAYAT_UUID=:CD_GRAMPANCHAYAT_UUID </if> ';
    const params = {};
    params.APP_LOGGED_IN_ROLE_ID = 20;
    params.CD_GRAMPANCHAYAT_UUID = '5657';
    const parsedQuery = __1.queryResolver(info_commons_1.queryParser(query, params), params);
    /*const knex: Knex = createKnex(config.get('db.appDB'));
    console.log("RESULT::::", await knex.raw(parsedQuery, params));*/
});
const createKnex = (dbConfig) => {
    const dialect = dbConfig.dialect;
    return knex_1.default({
        client: dialect,
        connection: {
            database: dbConfig.name,
            host: dbConfig.host,
            password: dbConfig.password,
            user: dbConfig.username,
        },
        pool: {
            max: dbConfig.pool.max,
            min: dbConfig.pool.min,
        },
        useNullAsDefault: true,
    });
};
//# sourceMappingURL=query.parser.test.js.map