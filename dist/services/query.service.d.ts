import { QueryResponse } from 'info-query';
declare class QueryService {
    executeNativeQuery(query: string, jwt: string, dataSource: string): Promise<QueryResponse>;
    executeCustomQuery(query: string, jwt: string): Promise<QueryResponse>;
}
declare const queryService: QueryService;
export default queryService;
//# sourceMappingURL=query.service.d.ts.map