"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_query_1 = require("info-query");
const sinon_1 = __importDefault(require("sinon"));
const __1 = require("../");
test('executeNativeQuery service test', async () => {
    const mockOutboundMDService = sinon_1.default.mock(info_query_1.outboundQueryMetadataService);
    mockOutboundMDService.expects('queryMetadata').returns({});
    const response = new info_query_1.QueryResponse('2019-07-16T14:16:06.314Z', 14, 'completed', 'ad-hoc', 1, 14, null);
    const mockGetMetadata = sinon_1.default.mock(info_query_1.queryExecutionService);
    mockGetMetadata.expects('executeQuery').returns(response);
    const queryResponse = await __1.queryService.executeNativeQuery('select * from vendor', 'dummyToken', 'mock');
    sinon_1.default.assert.match(queryResponse.status, 'completed');
    mockOutboundMDService.restore();
    mockGetMetadata.restore();
});
test('executeCustomQuery service test', async () => {
    const mockOutboundMDService = sinon_1.default.mock(info_query_1.outboundQueryMetadataService);
    mockOutboundMDService.expects('queryMetadata').returns({});
    const response = new info_query_1.QueryResponse('2019-07-16T14:16:06.314Z', 14, 'completed', 'ad-hoc', 1, 14, null);
    const mockGetMetadata = sinon_1.default.mock(info_query_1.queryExecutionService);
    mockGetMetadata.expects('executeQuery').returns(response);
    const customQuery = JSON.stringify({
        database: '11111111-1111-1111-1111-111111111111',
        query: {
            filter: [
                '=',
                [
                    'field-id', '1',
                ],
                1,
            ],
            source_table: '123',
        },
        type: 'query',
        isNative: false,
    });
    const queryResponse = await __1.queryService.executeCustomQuery(customQuery, 'dummyToken');
    sinon_1.default.assert.match(queryResponse.status, 'completed');
    mockOutboundMDService.restore();
    mockGetMetadata.restore();
});
//# sourceMappingURL=query.service.test.js.map