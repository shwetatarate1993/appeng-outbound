"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const commons = __importStar(require("info-commons"));
const info_query_1 = require("info-query");
const sinon_1 = __importDefault(require("sinon"));
const __1 = require("../");
test('singleSelect service native execution test with response data', async () => {
    const data = {
        LogicalEntity: {
            configObjectId: '2c651ef6-1c19-4087-ae8e-d5e1f1df91',
            physicalEntities: [{
                    configObjectId: '2cw51ef6-1c19-4087-ae8e-dwe771f1',
                    singleSelectQID: 'select * from vendor where vendor_id=:vendor_id',
                }],
        },
    };
    const mockGetMetadata = sinon_1.default.mock(commons);
    mockGetMetadata.expects('getMetaData').returns(data);
    const response = {
        started_at: '2019-07-19T10:35:09.420Z',
        average_execution_time: 204,
        status: 'completed',
        context: 'ad-hoc',
        row_count: 2,
        running_time: 204,
        data: {
            rows: [
                [
                    1,
                    'Ankur',
                ],
            ],
            columns: [
                'VENDOR_ID',
                'VENDOR_NAME',
            ],
            cols: [
                {
                    catalog: 'def',
                    db: 'prodrecruitmentapp',
                    table: 'vendor',
                    orgTable: 'vendor',
                    name: 'VENDOR_ID',
                    orgName: 'VENDOR_ID',
                    charsetNr: 63,
                    length: 10,
                    type: 3,
                    flags: 16387,
                    decimals: 0,
                    zeroFill: false,
                    protocol41: true,
                    display_name: 'Vendor Id',
                    base_type: 'type/Integer',
                },
                {
                    catalog: 'def',
                    db: 'prodrecruitmentapp',
                    table: 'vendor',
                    orgTable: 'vendor',
                    name: 'VENDOR_NAME',
                    orgName: 'VENDOR_NAME',
                    charsetNr: 33,
                    length: 150,
                    type: 253,
                    flags: 128,
                    decimals: 0,
                    zeroFill: false,
                    protocol41: true,
                    display_name: 'Vendor Name',
                    base_type: 'type/Text',
                },
            ],
        },
    };
    const mockQueryService = sinon_1.default.mock(__1.queryService);
    mockQueryService.expects('executeNativeQuery').returns(response);
    const res = await __1.outBoundService.singleSelect('LogicalEntityName', '1', 'dummyToken', 'dummyToken', '');
    sinon_1.default.assert.match(res.VENDOR_ID, 1);
    sinon_1.default.assert.match(res.VENDOR_NAME, 'Ankur');
    mockGetMetadata.restore();
    mockQueryService.restore();
});
test('singleSelect service test with null response', async () => {
    const details = {
        data: {
            LogicalEntity: null,
        },
    };
    const mockGetMetadata = sinon_1.default.mock(commons);
    mockGetMetadata.expects('getMetaData').returns(details);
    const res = await __1.outBoundService.fetchGridData('LogicalEntityName', {}, 'abc', 'dummyToken', '');
    sinon_1.default.assert.match(res, null);
    mockGetMetadata.restore();
});
test('singleSelect service custom query execution test with response data', async () => {
    const data = {
        LogicalEntity: {
            configObjectId: '2c651ef6-1c19-4087-ae8e-d5e1f1df91',
            physicalEntities: [{
                    configObjectId: '2cw51ef6-1c19-4087-ae8e-dwe771f1',
                    singleSelectQID: '',
                    physicalColumns: [{
                            configObjectId: '2cw51ef6-1c19-4087-ae8e',
                            isPrimaryKey: true,
                            dbCode: 'VENDOR_ID',
                        },
                        {
                            configObjectId: '2cw51ef6-1c19-4087-ae8e-25633578',
                            isPrimaryKey: false,
                            dbCode: 'VENDOR_NAME',
                        }],
                }],
        },
    };
    const mockGetMetadata = sinon_1.default.mock(commons);
    mockGetMetadata.expects('getMetaData').returns(data);
    const response = {
        started_at: '2019-07-19T10:35:09.420Z',
        average_execution_time: 204,
        status: 'completed',
        context: 'ad-hoc',
        row_count: 2,
        running_time: 204,
        data: {
            rows: [
                [
                    1,
                    'Ankur',
                ],
            ],
            columns: [
                'VENDOR_ID',
                'VENDOR_NAME',
            ],
            cols: [
                {
                    catalog: 'def',
                    db: 'prodrecruitmentapp',
                    table: 'vendor',
                    orgTable: 'vendor',
                    name: 'VENDOR_ID',
                    orgName: 'VENDOR_ID',
                    charsetNr: 63,
                    length: 10,
                    type: 3,
                    flags: 16387,
                    decimals: 0,
                    zeroFill: false,
                    protocol41: true,
                    display_name: 'Vendor Id',
                    base_type: 'type/Integer',
                },
                {
                    catalog: 'def',
                    db: 'prodrecruitmentapp',
                    table: 'vendor',
                    orgTable: 'vendor',
                    name: 'VENDOR_NAME',
                    orgName: 'VENDOR_NAME',
                    charsetNr: 33,
                    length: 150,
                    type: 253,
                    flags: 128,
                    decimals: 0,
                    zeroFill: false,
                    protocol41: true,
                    display_name: 'Vendor Name',
                    base_type: 'type/Text',
                },
            ],
        },
    };
    const mockQueryService = sinon_1.default.mock(__1.queryService);
    mockQueryService.expects('executeCustomQuery').returns(response);
    const res = await __1.outBoundService.singleSelect('LogicalEntityName', '1', 'dummyToken', 'dummyToken', '');
    sinon_1.default.assert.match(res.VENDOR_ID, 1);
    sinon_1.default.assert.match(res.VENDOR_NAME, 'Ankur');
    mockGetMetadata.restore();
    mockQueryService.restore();
});
test('fetchGridData service test with response data', async () => {
    const data = {
        LogicalEntity: {
            configObjectId: '2c651ef6-1c19-4087-ae8e-d5e771f1df91',
            dataGrids: [{
                    configObjectId: '2c651ef6-1c19-4087-ae8e-d5e771f1',
                    logicalEntityOperations: [{
                            gridSelectId: 'select * from vendor',
                        }],
                }],
        },
    };
    const mockGetMetadata = sinon_1.default.mock(commons);
    mockGetMetadata.expects('getMetaData').returns(data);
    const response = new info_query_1.QueryResponse('2019-07-16T14:16:06.314Z', 14, 'completed', 'ad-hoc', 1, 14, null);
    const mockQueryService = sinon_1.default.mock(__1.queryService);
    mockQueryService.expects('executeNativeQuery').returns(response);
    const res = await __1.outBoundService.fetchGridData('LogicalEntityName', {}, 'abc', 'dummyToken', '');
    sinon_1.default.assert.match(res.status, 'completed');
    mockGetMetadata.restore();
    mockQueryService.restore();
});
test('fetchGridData service test with null response', async () => {
    const details = {
        data: {
            LogicalEntity: null,
        },
    };
    const mockGetMetadata = sinon_1.default.mock(commons);
    mockGetMetadata.expects('getMetaData').returns(details);
    const res = await __1.outBoundService.fetchGridData('LogicalEntityName', {}, 'abc', 'abc', '');
    sinon_1.default.assert.match(res, null);
    mockGetMetadata.restore();
});
test('executeMultiSelectQuery service test with response data', async () => {
    const data = {
        LogicalEntity: {
            configObjectId: '2c651ef6-1c19-4087-ae8e-d5e771f1df91',
            physicalEntities: [{
                    configObjectId: '1111ef6-1c19-4087-ae8e-d5e771f1df91',
                    multiSelectQID: 'SELECT * FROM vendor_contacts WHERE VENDOR_ID=:VENDOR_ID and ISDELETED!=1',
                }],
        },
    };
    const mockGetMetadata = sinon_1.default.mock(commons);
    mockGetMetadata.expects('getMetaData').returns(data);
    const response = new info_query_1.QueryResponse('2019-07-16T14:16:06.314Z', 14, 'completed', 'ad-hoc', 1, 14, null);
    const mockQueryService = sinon_1.default.mock(__1.queryService);
    mockQueryService.expects('executeNativeQuery').returns(response);
    const res = await __1.outBoundService.executeMultiSelectQuery('LogicalEntityName', { VENDOR_ID: '203' }, 'abc', 'abc', '');
    sinon_1.default.assert.match(res.status, 'completed');
    mockGetMetadata.restore();
    mockQueryService.restore();
});
test('executeMultiSelectQuery service test with null response', async () => {
    const details = {
        data: {
            LogicalEntity: null,
        },
    };
    const mockGetMetadata = sinon_1.default.mock(commons);
    mockGetMetadata.expects('getMetaData').returns(details);
    const res = await __1.outBoundService.executeMultiSelectQuery('LogicalEntityName', {}, 'abc', 'abc', '');
    sinon_1.default.assert.match(res, null);
    mockGetMetadata.restore();
});
//# sourceMappingURL=outbound.service.test.js.map