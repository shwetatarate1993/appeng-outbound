"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const info_commons_2 = require("info-commons");
const info_query_1 = require("info-query");
class QueryService {
    async executeNativeQuery(query, jwt, dataSource) {
        query = info_commons_1.queryExtractor(query, info_commons_2.DIALECT);
        const nativeQuery = {
            database: '11111111-1111-1111-1111-111111111111',
            native: {
                query,
            },
            type: 'native',
            isNative: true,
            datasource: dataSource,
        };
        const queryMetadata = new info_query_1.QueryMetadata(nativeQuery);
        await queryMetadata.init(jwt);
        const queryResponse = await info_query_1.queryExecutionService.executeQuery(queryMetadata, jwt);
        return queryResponse;
    }
    async executeCustomQuery(query, jwt) {
        const queryMetadata = new info_query_1.QueryMetadata(JSON.parse(query));
        await queryMetadata.init(jwt);
        const queryResponse = await info_query_1.queryExecutionService.executeQuery(queryMetadata, jwt);
        return queryResponse;
    }
}
const queryService = new QueryService();
// Object.freeze(queryService);
exports.default = queryService;
//# sourceMappingURL=query.service.js.map