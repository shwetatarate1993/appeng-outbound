import { QueryResponse } from 'info-query';
export declare class OutBoundService {
    singleSelect(logicalEntityName: string, primaryKeyValue: string, authToken: string, userDetailsToken: string, provider: string): Promise<any>;
    fetchGridData(logicalEntityName: string, requestBody: any, authToken: string, userDetailsToken: string, provider: string): Promise<QueryResponse>;
    fetchFormDataByGridId(datagridId: string, primaryKeyValue: string, authToken: string, userDetailsToken: string, provider: string): Promise<any>;
    fetchGridDataByGridId(datagridId: string, requestBody: any, authToken: string, userDetailsToken: string, provider: string): Promise<QueryResponse>;
    fetchSelectOptionData(formFieldName: string, requestBody: any, authToken: string, userDetailsToken: string, provider: string): Promise<QueryResponse>;
    executeMultiSelectQuery(logicalEntityName: string, requestBody: any, authToken: string, userDetailsToken: string, provider: string): Promise<QueryResponse>;
}
declare const outBoundService: OutBoundService;
export default outBoundService;
//# sourceMappingURL=outbound.service.d.ts.map