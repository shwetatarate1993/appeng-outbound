"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const _1 = require(".");
const configitem_graphql_query_1 = require("../queries/configitem.graphql.query");
const utilities_1 = require("../utilities");
/* tslint:disable-next-line:no-var-requires */
const fetch = require('node-fetch');
const info_commons_2 = require("info-commons");
const jwt = __importStar(require("jsonwebtoken"));
class OutBoundService {
    async singleSelect(logicalEntityName, primaryKeyValue, authToken, userDetailsToken, provider) {
        const errorResponse = await info_commons_2.validateToken(authToken, provider);
        if (errorResponse !== null) {
            return errorResponse;
        }
        const data = await utilities_1.getMetaData(info_commons_1.APPENG_META_URI, logicalEntityName, configitem_graphql_query_1.queryDataGrid, authToken);
        if (data.LogicalEntity.dataGrids[0].logicalEntityOperations[0].selectId) {
            let singleSelectQueryResult = data.LogicalEntity.dataGrids[0].logicalEntityOperations[0].selectId;
            const dataSource = data.LogicalEntity.dataGrids[0].logicalEntityOperations[0].workAreaSessName;
            let params = { entityPrimaryKey: primaryKeyValue };
            params = getParameters(userDetailsToken, params);
            const response = await _1.queryService.executeNativeQuery(parseQuery(singleSelectQueryResult, params), authToken, dataSource);
            return utilities_1.formatResponse(response);
        }
        else {
            // const singleSelectQuery: string =
            //     await singleSelectQueryGenerator.generateSingleSelectQuery(data, primaryKeyValue);
            // const res: QueryResponse = await queryService.executeCustomQuery(singleSelectQuery, authToken);
            // return formatResponse(res);
            return null;
        }
    }
    async fetchGridData(logicalEntityName, requestBody, authToken, userDetailsToken, provider) {
        const errorResponse = await info_commons_2.validateToken(authToken, provider);
        if (errorResponse !== null) {
            return errorResponse;
        }
        const params = getParameters(userDetailsToken, requestBody);
        const data = await utilities_1.getMetaData(info_commons_1.APPENG_META_URI, logicalEntityName, configitem_graphql_query_1.queryDataGrid, authToken);
        let gridSelectQuery = '';
        let dataSource = '';
        try {
            gridSelectQuery = data.LogicalEntity.dataGrids[0].logicalEntityOperations[0].gridSelectId;
            dataSource = data.LogicalEntity.dataGrids[0].logicalEntityOperations[0].workAreaSessName;
        }
        catch (err) {
            return null;
        }
        return await _1.queryService.executeNativeQuery(parseQuery(gridSelectQuery, params), authToken, dataSource);
    }
    async fetchFormDataByGridId(datagridId, primaryKeyValue, authToken, userDetailsToken, provider) {
        const errorResponse = await info_commons_2.validateToken(authToken, provider);
        if (errorResponse !== null) {
            return errorResponse;
        }
        const data = await utilities_1.getMetaData(info_commons_1.APPENG_META_URI, datagridId, configitem_graphql_query_1.fetchDataGridById, authToken);
        let singleSelectQueryResult = '';
        let dataSource = '';
        try {
            singleSelectQueryResult = data.DataGrid.logicalEntityOperations[0].selectId;
            dataSource = data.DataGrid.logicalEntityOperations[0].workAreaSessName;
        }
        catch (err) {
            return null;
        }
        let params = { entityPrimaryKey: primaryKeyValue };
        params = getParameters(userDetailsToken, params);
        const response = await _1.queryService.executeNativeQuery(parseQuery(singleSelectQueryResult, params), authToken, dataSource);
        return utilities_1.formatResponse(response);
    }
    async fetchGridDataByGridId(datagridId, requestBody, authToken, userDetailsToken, provider) {
        const errorResponse = await info_commons_2.validateToken(authToken, provider);
        if (errorResponse !== null) {
            return errorResponse;
        }
        const params = getParameters(userDetailsToken, requestBody);
        const data = await utilities_1.getMetaData(info_commons_1.APPENG_META_URI, datagridId, configitem_graphql_query_1.fetchDataGridById, authToken);
        let gridSelectQuery = '';
        let dataSource = '';
        try {
            gridSelectQuery = data.DataGrid.logicalEntityOperations[0].gridSelectId;
            dataSource = data.DataGrid.logicalEntityOperations[0].workAreaSessName;
        }
        catch (err) {
            return null;
        }
        return await _1.queryService.executeNativeQuery(parseQuery(gridSelectQuery, params), authToken, dataSource);
    }
    async fetchSelectOptionData(formFieldName, requestBody, authToken, userDetailsToken, provider) {
        const errorResponse = await info_commons_2.validateToken(authToken, provider);
        if (errorResponse !== null) {
            return errorResponse;
        }
        const params = getParameters(userDetailsToken, requestBody);
        const data = await utilities_1.getMetaData(info_commons_1.APPENG_META_URI, formFieldName, configitem_graphql_query_1.querySelectOption, authToken);
        let selectQuery = '';
        let dataSource = '';
        try {
            selectQuery = data.FormField.selectItemsReferenceID;
            dataSource = data.FormField.dataSourceName;
        }
        catch (err) {
            return null;
        }
        return await _1.queryService.executeNativeQuery(parseQuery(selectQuery, params), authToken, dataSource);
    }
    async executeMultiSelectQuery(logicalEntityName, requestBody, authToken, userDetailsToken, provider) {
        const errorResponse = await info_commons_2.validateToken(authToken, provider);
        if (errorResponse !== null) {
            return errorResponse;
        }
        const params = getParameters(userDetailsToken, requestBody);
        const data = await utilities_1.getMetaData(info_commons_1.APPENG_META_URI, logicalEntityName, configitem_graphql_query_1.queryLogicalEntity, authToken);
        let multiSelectQuery = '';
        let dataSource = '';
        try {
            sortPhysicalEntities(data.LogicalEntity.physicalEntities);
            multiSelectQuery = data.LogicalEntity.physicalEntities[0].multiSelectQID;
            dataSource = data.LogicalEntity.physicalEntities[0].workAreaSessName;
            if (data.LogicalEntity.physicalEntities.length > 1
                && !data.LogicalEntity.physicalEntities[0].isPrimaryEntity) {
                const primaryPE = data.LogicalEntity.physicalEntities.find((pe) => pe.isPrimaryEntity);
                if (primaryPE) {
                    multiSelectQuery = primaryPE.multiSelectQID;
                    dataSource = primaryPE.workAreaSessName;
                }
            }
        }
        catch (err) {
            return null;
        }
        if (!multiSelectQuery || multiSelectQuery.trim() === '0') {
            return null;
        }
        return await _1.queryService.executeNativeQuery(parseQuery(multiSelectQuery, params), authToken, dataSource);
    }
}
exports.OutBoundService = OutBoundService;
const getParameters = (userDetailsToken, requestBody) => {
    const payload = jwt.verify(userDetailsToken, info_commons_2.config.get(info_commons_2.SECRET));
    const params = Object.assign(payload, requestBody);
    return params;
};
const parseQuery = (query, params) => {
    params = info_commons_1.putMissingdbCodesinParams(query, params);
    if (query.includes('</if>')) {
        query = info_commons_1.queryParser(query, params);
    }
    if (query.includes(':')) {
        query = utilities_1.queryResolver(query, params);
    }
    return query;
};
const sortPhysicalEntities = (physicalEntities) => {
    physicalEntities.sort((a, b) => {
        return a.order - b.order;
    });
};
const outBoundService = new OutBoundService();
// Object.freeze(outBoundService);
exports.default = outBoundService;
//# sourceMappingURL=outbound.service.js.map