export declare enum DBEngine {
    SQLLITE = "sqlite3",
    MYSQL = "mysql"
}
export declare const parseDBEngineEnum: (dbEngine: string) => DBEngine;
//# sourceMappingURL=dbengine.enum.d.ts.map