"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TOKEN_KEY = 'x-access-token';
exports.DB = 'db';
var dbengine_enum_1 = require("./dbengine.enum");
exports.DBEngine = dbengine_enum_1.DBEngine;
exports.parseDBEngineEnum = dbengine_enum_1.parseDBEngineEnum;
//# sourceMappingURL=index.js.map