"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SingleSelectQueryGenerator {
    async generateSingleSelectQuery(data, primaryKeyValue) {
        const physicalColumn = data.LogicalEntity.physicalEntities[0].physicalColumns
            .find((column) => column.isPrimaryKey);
        const physicalEntity = data.LogicalEntity.physicalEntities[0];
        return JSON.stringify({
            database: '11111111-1111-1111-1111-111111111111',
            query: {
                filter: [
                    '=',
                    [
                        'field-id', physicalColumn.configObjectId,
                    ],
                    primaryKeyValue,
                ],
                source_table: physicalEntity.configObjectId,
            },
            type: 'query',
            isNative: false,
        });
    }
}
const singleSelectQueryGenerator = new SingleSelectQueryGenerator();
Object.freeze(singleSelectQueryGenerator);
exports.default = singleSelectQueryGenerator;
//# sourceMappingURL=single.select.query.generator.js.map