import { QueryResponse } from 'info-query';
export default interface CustomQueryGenerator {
    generateSingleSelectQuery(data: any, primaryKeyValue: string): Promise<QueryResponse>;
}
//# sourceMappingURL=custom.query.generator.interface.d.ts.map