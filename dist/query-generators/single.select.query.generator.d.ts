declare class SingleSelectQueryGenerator {
    generateSingleSelectQuery(data: any, primaryKeyValue: string): Promise<string>;
}
declare const singleSelectQueryGenerator: SingleSelectQueryGenerator;
export default singleSelectQueryGenerator;
//# sourceMappingURL=single.select.query.generator.d.ts.map