"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const __1 = require("../");
test('executeNativeQuery service test', async () => {
    const data = {
        LogicalEntity: {
            configObjectId: '2c651ef6-1c19-4087-ae8e-d5e1f1df91',
            physicalEntities: [{
                    configObjectId: '2cw51ef6-1c19-4087-ae8e-dwe771f1',
                    singleSelectQID: '',
                    physicalColumns: [{
                            configObjectId: '2cw51ef6-1c19-4087-ae8e',
                            isPrimaryKey: true,
                            dbCode: 'VENDOR_ID',
                        },
                        {
                            configObjectId: '2cw51ef6-1c19-4087-ae8e-25633578',
                            isPrimaryKey: false,
                            dbCode: 'VENDOR_NAME',
                        }],
                }],
        },
    };
    const customQuery = JSON.parse(await __1.singleSelectQueryGenerator.generateSingleSelectQuery(data, '1'));
    sinon_1.default.assert.match(customQuery.query.filter.length, 3);
    sinon_1.default.assert.match(customQuery.query.filter[0], '=');
    sinon_1.default.assert.match(customQuery.query.filter[1][1], '2cw51ef6-1c19-4087-ae8e');
    sinon_1.default.assert.match(customQuery.query.source_table, '2cw51ef6-1c19-4087-ae8e-dwe771f1');
});
//# sourceMappingURL=single.select.query.generator.test.js.map