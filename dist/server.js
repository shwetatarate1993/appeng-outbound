"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const body_parser_1 = __importDefault(require("body-parser"));
const compression_1 = __importDefault(require("compression"));
const cors_1 = __importDefault(require("cors"));
const errorhandler_1 = __importDefault(require("errorhandler"));
const express_1 = __importDefault(require("express"));
const info_commons_1 = require("info-commons");
const config_1 = __importDefault(require("./config"));
const init_config_1 = require("./init-config");
const routes_1 = __importDefault(require("./routes"));
init_config_1.AppengOutboundConfig.configure(config_1.default.get('db'), info_commons_1.DIALECT);
const app = express_1.default();
app.use(compression_1.default());
app.use(body_parser_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use(cors_1.default());
app.use('/v1', routes_1.default.router);
app.get('/', (req, res) => res.send('Hello Outbound Service!'));
if (process.env.NODE_ENV === 'development') {
    app.use(errorhandler_1.default());
}
const server = app.listen(config_1.default.get('port'));
exports.default = server;
//# sourceMappingURL=server.js.map