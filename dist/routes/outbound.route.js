"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const controllers_1 = require("../controllers");
const router = express_1.default.Router();
router.get('/formData/:datagridId/:primaryKeyValue', controllers_1.outBoundController.fetchFormDataByGridId);
router.post('/gridData/:datagridId', controllers_1.outBoundController.fetchGridDataByGridId);
router.get('/:logicalEntityName/:primaryKeyValue', controllers_1.outBoundController.singleSelect);
router.post('/:logicalEntityName/dataGrid', controllers_1.outBoundController.fetchGridData);
router.post('/:formFieldName/option', controllers_1.outBoundController.fetchSelectOptionData);
router.post('/:logicalEntityName/multiSelect', controllers_1.outBoundController.executeMultiSelectQuery);
exports.default = router;
//# sourceMappingURL=outbound.route.js.map