import { default as convict } from 'convict';
declare const config: convict.Config<{
    db: {
        mock: any;
        PRIMARYSPRING: any;
        PRIMARY_MD_SPRING: any;
    };
    provider: string;
    env: string;
    ip: string;
    port: number;
}>;
export default config;
//# sourceMappingURL=index.d.ts.map