"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const convict_1 = __importDefault(require("convict"));
const development_1 = __importDefault(require("./env/development"));
const production_1 = __importDefault(require("./env/production"));
// Define a schema
const config = convict_1.default({
    db: {
        mock: {
            sqlite3: {
                dialect: 'sqlite3',
                connection: {
                    doc: 'SQL Lite connection type',
                    format: '*',
                    default: ':memory:',
                },
            },
        },
        PRIMARYSPRING: {
            mysql: {
                dialect: 'mysql',
                host: {
                    doc: 'Database host name/IP',
                    format: '*',
                    default: 'awsmysqldev1.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
                },
                name: {
                    doc: 'Database name',
                    format: String,
                    default: 'villageportalappdev',
                },
                username: {
                    doc: 'Database Username',
                    format: String,
                    default: 'awsdevmaster',
                },
                password: {
                    doc: 'Database Password',
                    format: String,
                    default: 'awsinfo2018',
                },
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000,
                },
            },
            sqlite3: {
                dialect: 'sqlite3',
                connection: {
                    filename: 'vpdata.db',
                },
            },
        },
        PRIMARY_MD_SPRING: {
            mysql: {
                dialect: 'mysql',
                host: {
                    doc: 'Database host name/IP',
                    format: '*',
                    default: 'awsmysqldev1.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
                },
                name: {
                    doc: 'Database name',
                    format: String,
                    default: 'vpmd',
                },
                username: {
                    doc: 'Database Username',
                    format: String,
                    default: 'awsdevmaster',
                },
                password: {
                    doc: 'Database Password',
                    format: String,
                    default: 'awsinfo2018',
                },
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000,
                },
            },
            sqlite3: {
                dialect: 'sqlite3',
                connection: {
                    filename: 'vpdata.db',
                },
            },
        },
    },
    provider: {
        default: 'default',
    },
    env: {
        doc: 'The application environment.',
        format: ['production', 'development', 'test'],
        default: 'development',
        env: 'NODE_ENV',
    },
    ip: {
        doc: 'The IP address to bind.',
        format: 'ipaddress',
        default: '127.0.0.1',
        env: 'IP_ADDRESS',
    },
    port: {
        doc: 'The port to bind.',
        format: 'port',
        default: 8898,
        env: 'PORT',
        arg: 'port',
    },
});
// Load environment dependent configuration
switch (config.get('env')) {
    case 'production':
        config.load(production_1.default);
        break;
    case 'development':
        config.load(development_1.default);
        break;
    default:
    // do nothing
}
// Perform validation
config.validate({ allowed: 'strict' });
exports.default = config;
//# sourceMappingURL=index.js.map