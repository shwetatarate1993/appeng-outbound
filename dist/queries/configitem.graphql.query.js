"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_tag_1 = __importDefault(require("graphql-tag"));
exports.querySelectOption = graphql_tag_1.default `
    query FormField($id:ID!){
        FormField(id:$id) {
            configObjectId
            selectItemsReferenceID
            dataSourceName
    }
}`;
exports.queryDataGrid = graphql_tag_1.default `
    query LogicalEntity($id:ID!){
    LogicalEntity(id:$id) {
        configObjectId
        dataGrids {
            configObjectId
            logicalEntityOperations {
              gridSelectId
              selectId
              workAreaSessName
            }
        }
    }
}`;
exports.fetchDataGridById = graphql_tag_1.default `
    query DataGrid($id:ID!){
        DataGrid(id:$id) {
        configObjectId
        logicalEntityOperations {
          gridSelectId
          selectId
          workAreaSessName
        }
    }
}`;
exports.queryLogicalEntity = graphql_tag_1.default `
    query LogicalEntity($id:ID!){
    LogicalEntity(id:$id) {
        configObjectId
        physicalEntities {
            configObjectId
            singleSelectQID
            multiSelectQID
            name
            isPrimaryEntity
            workAreaSessName
            physicalColumns{
                configObjectId
                isPrimaryKey
                dbCode
            }
        }
    }
}`;
//# sourceMappingURL=configitem.graphql.query.js.map