declare const _default: ({
    description: string;
    features: string[];
    name: string;
    tables: {
        description: string;
        schema: string;
        active: boolean;
        name: string;
        rows: number;
        updated_at: string;
        id: number;
        db_id: number;
        visibility_type: string;
        display_name: string;
        created_at: string;
    }[];
    updated_at: string;
    native_permissions: string;
    details: {
        client: string;
        connection: string;
        'host'?: undefined;
        'port'?: undefined;
        'dbname'?: undefined;
        'user'?: undefined;
        'password'?: undefined;
        'tunnel-port'?: undefined;
        db?: undefined;
    };
    id: number;
    engine: string;
    created_at: string;
    is_saved_questions?: undefined;
} | {
    description: string;
    features: string[];
    name: string;
    tables: {
        description: string;
        schema: string;
        active: boolean;
        name: string;
        rows: number;
        updated_at: string;
        id: number;
        db_id: string;
        visibility_type: string;
        display_name: string;
        created_at: string;
    }[];
    updated_at: string;
    native_permissions: string;
    details: {
        client: string;
        connection: string;
        'host'?: undefined;
        'port'?: undefined;
        'dbname'?: undefined;
        'user'?: undefined;
        'password'?: undefined;
        'tunnel-port'?: undefined;
        db?: undefined;
    };
    id: string;
    engine: string;
    created_at: string;
    is_saved_questions?: undefined;
} | {
    description: string;
    features: string[];
    name: string;
    tables: {
        description: string;
        name: string;
        rows: number;
        updated_at: string;
        active: boolean;
        id: number;
        db_id: number;
        visibility_type: string;
        display_name: string;
        created_at: string;
        schema: string;
    }[];
    updated_at: string;
    native_permissions: string;
    details: {
        'host': string;
        'port': number;
        'dbname': string;
        'user': string;
        'password': string;
        'tunnel-port': number;
        client?: undefined;
        connection?: undefined;
        db?: undefined;
    };
    id: number;
    engine: string;
    created_at: string;
    is_saved_questions?: undefined;
} | {
    description: string;
    features: string[];
    name: string;
    tables: {
        description: string;
        schema: string;
        active: boolean;
        name: string;
        rows: number;
        updated_at: string;
        id: number;
        db_id: number;
        visibility_type: string;
        display_name: string;
        created_at: string;
    }[];
    updated_at: string;
    native_permissions: string;
    details: {
        db: string;
        client?: undefined;
        connection?: undefined;
        'host'?: undefined;
        'port'?: undefined;
        'dbname'?: undefined;
        'user'?: undefined;
        'password'?: undefined;
        'tunnel-port'?: undefined;
    };
    id: number;
    engine: string;
    created_at: string;
    is_saved_questions?: undefined;
} | {
    name: string;
    id: number;
    features: string[];
    tables: {
        id: string;
        db_id: number;
        display_name: string;
        schema: string;
        description: string;
    }[];
    is_saved_questions: boolean;
    description?: undefined;
    updated_at?: undefined;
    native_permissions?: undefined;
    details?: undefined;
    engine?: undefined;
    created_at?: undefined;
})[];
export default _default;
//# sourceMappingURL=databases.d.ts.map