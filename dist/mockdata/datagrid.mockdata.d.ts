declare const _default: {
    data: {
        DataGrid: {
            configObjectId: string;
            defaultOrdering: boolean;
            gridType: any;
            isHavingAdvanceFilterForm: boolean;
            scroll: boolean;
            swimlaneRequired: boolean;
            modalRequired: boolean;
            isRowReOrder: boolean;
            name: string;
            configObjectType: string;
            createdBy: string;
            isDeleted: number;
            creationDate: string;
            projectId: number;
            itemDescription: any;
            updatedBy: any;
            updationDate: any;
            deletionDate: any;
            logicalEntityOperations: {
                configObjectId: string;
                name: string;
                configObjectType: string;
                selectId: any;
                gridSelectId: string;
                filteredQuery: any;
                workAreaSessName: string;
                createdBy: string;
                isDeleted: number;
                itemDescription: any;
                creationDate: string;
                projectId: any;
                updatedBy: any;
                updationDate: any;
                deletionDate: any;
            }[];
        };
    };
};
export default _default;
//# sourceMappingURL=datagrid.mockdata.d.ts.map