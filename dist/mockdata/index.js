"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const databases_1 = __importDefault(require("./databases"));
exports.databases = databases_1.default;
const datagrid_mockdata_1 = __importDefault(require("./datagrid.mockdata"));
exports.dataGrid = datagrid_mockdata_1.default;
const grid_result_mockdata_1 = __importDefault(require("./grid.result.mockdata"));
exports.gridResult = grid_result_mockdata_1.default;
const physical_entity_1 = __importDefault(require("./physical.entity"));
exports.entities = physical_entity_1.default;
var logical_entity_mockdata_1 = require("./logical.entity.mockdata");
exports.logicalEntity = logical_entity_mockdata_1.default;
//# sourceMappingURL=index.js.map