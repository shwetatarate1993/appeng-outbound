declare const _default: {
    started_at: string;
    average_execution_time: number;
    status: string;
    context: string;
    row_count: number;
    running_time: number;
    data: {
        rows: (string | number)[][];
        columns: any[];
        cols: any[];
    };
};
export default _default;
//# sourceMappingURL=grid.result.mockdata.d.ts.map