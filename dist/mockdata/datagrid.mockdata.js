"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    data: {
        DataGrid: {
            configObjectId: '14212216-df9c-451a-aac1-496f88d7629f',
            defaultOrdering: true,
            gridType: null,
            isHavingAdvanceFilterForm: false,
            scroll: false,
            swimlaneRequired: false,
            modalRequired: false,
            isRowReOrder: false,
            name: 'DataGrid',
            configObjectType: 'DataGrid',
            createdBy: '1119',
            isDeleted: 0,
            creationDate: '2019-04-05T10:40:06.000Z',
            projectId: 0,
            itemDescription: null,
            updatedBy: null,
            updationDate: null,
            deletionDate: null,
            logicalEntityOperations: [
                {
                    configObjectId: '7bw2ceb-cbcb-43ae-84ad-b28548f579b',
                    name: 'LogicalEntityOperation',
                    configObjectType: 'LogicalEntityOperation',
                    selectId: null,
                    gridSelectId: 'select * from vendor where vendor_id=1',
                    filteredQuery: null,
                    workAreaSessName: 'PRIMARY',
                    createdBy: '1119',
                    isDeleted: 0,
                    itemDescription: null,
                    creationDate: '2019-04-05T10:40:06.000Z',
                    projectId: null,
                    updatedBy: null,
                    updationDate: null,
                    deletionDate: null,
                },
            ],
        },
    },
};
//# sourceMappingURL=datagrid.mockdata.js.map