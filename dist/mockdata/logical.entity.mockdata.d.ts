declare const _default: {
    "LogicalEntity": {
        "configObjectId": string;
        "dataGrids": {
            "configObjectId": string;
            "logicalEntityOperations": {
                "gridSelectId": string;
                "selectId": string;
                "workAreaSessName": string;
            }[];
        }[];
    };
};
export default _default;
//# sourceMappingURL=logical.entity.mockdata.d.ts.map