"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    "LogicalEntity": {
        "configObjectId": "00db3413-7b22-4115-bad6-5075c4ba48ff",
        "dataGrids": [
            {
                "configObjectId": "9a250c47-b9b5-4ae2-ba40-c50b775ad753",
                "logicalEntityOperations": [
                    {
                        "gridSelectId": "mysql!lAnGuAge!SELECT RESIDENT_UUID, RESIDENT_ID, STATE_ID, DISTRICT_ID, TALUKA_ID, GRAMPANCHAYAT_ID, RESIDENT_FIRST_NAME, RESIDENT_LAST_NAME, AADHAR_ID, RESIDENT_ADDRESS, MOBILE_NUMBER, PHONE_NUMBER, EMAIL_ADDRESS, DATE_OF_BIRTH, CREATEDBY, UPDATEDBY, CREATIONDATE, UPDATIONDATE, ISDELETED, DELETIONDATE, CHANGE_TIMESTAMP FROM RESIDENT WHERE ISDELETED!=1  <if test=\"APP_LOGGED_IN_ROLE_ID==17\">\t\t   and \t   GRAMPANCHAYAT_ID=:APP_LOGGED_IN_USER_GP </if>\t   <if test=\"APP_LOGGED_IN_ROLE_ID==18\">\t\t   and \t\tTALUKA_ID=:APP_LOGGED_IN_USER_TALUKA \t\t\t</if>\t   <if test=\"APP_LOGGED_IN_ROLE_ID==19\">\t\t   and DISTRICT_ID=:APP_LOGGED_IN_USER_DISTRICT    </if>\t   <if test=\"APP_LOGGED_IN_ROLE_ID==20\">\t\t   and STATE_ID=:APP_LOGGED_IN_USER_STATE \t\t \t</if>",
                        "selectId": "mysql!lAnGuAge!SELECT RESIDENT_UUID, RESIDENT_ID, STATE_ID, DISTRICT_ID, TALUKA_ID, GRAMPANCHAYAT_ID, RESIDENT_FIRST_NAME, RESIDENT_LAST_NAME, AADHAR_ID, RESIDENT_ADDRESS, MOBILE_NUMBER, PHONE_NUMBER, EMAIL_ADDRESS, DATE_OF_BIRTH, CREATEDBY, UPDATEDBY, CREATIONDATE, UPDATIONDATE, ISDELETED, DELETIONDATE, CHANGE_TIMESTAMP FROM RESIDENT where RESIDENT_UUID=:entityPrimaryKey",
                        "workAreaSessName": "PRIMARYSPRING"
                    }
                ]
            }
        ]
    }
};
//# sourceMappingURL=logical.entity.mockdata.js.map