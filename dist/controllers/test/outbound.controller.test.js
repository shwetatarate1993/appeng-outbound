"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const commons = __importStar(require("info-commons"));
const info_query_1 = require("info-query");
const node_mocks_http_1 = __importDefault(require("node-mocks-http"));
const sinon_1 = __importDefault(require("sinon"));
const __1 = require("../");
const services_1 = require("../../services");
beforeEach(() => {
    this.mock = sinon_1.default.mock(commons);
    this.mock.expects('validateToken').returns(null);
});
afterEach(() => {
    this.mock.restore();
});
// Creating mock req,res object
const req = node_mocks_http_1.default.createRequest({
    headers: {
        // tslint:disable-next-line:max-line-length
        authorization: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1NjIzMDU3NzYsImV4cCI6MTU5Mzg0MTc3NiwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSIsIkFQUF9MT0dHRURfSU5fU1VCX1BST0pFQ1RfSUQiOiIxIiwiQVBQX0xPR0dFRF9JTl9ZRUFSIjoiMSIsIkFQUF9MT0dHRURfSU5fVVNFUl9ESVNUUklDVCI6Ijk0MDgwM2U5LTU2YTktMTFlOS1iMDM5LTBlZTcyYzZkZGNlNiIsIkFQUF9DT05GSUdVUkFUSU9OX1BST0pFQ1RfSUQiOiIxIiwiQVBQX0xPR0dFRF9JTl9QUk9KRUNUX0lEIjoiMSIsIkFQUF9MT0dHRURfSU5fVVNFUl9TVEFURSI6ImU4YTE4ZWEyLTU2YTctMTFlOS1iMDM5LTBlZTcyYzZkZGNlNiIsIkFQUF9MT0dHRURfSU5fUk9MRV9JRCI6IjE5IiwiQVBQX0xPR0dFRF9JTl9ZRUFSX05BTUUiOiIyMDE0LTIwMTUiLCJBUFBfTE9HR0VEX0lOX1VTRVJfSUQiOiIxMTM3In0.gRiXPiT7vYiroWzNRm7rB4SrhA21DrnRXvrU4JF3kKE',
    },
});
test('singleSelect controller test', async () => {
    const res = node_mocks_http_1.default.createResponse();
    const response = { UUID: '1', NAME: 'Ankur' };
    const mockService = sinon_1.default.mock(services_1.outBoundService);
    mockService.expects('singleSelect').returns(response);
    await __1.outBoundController.singleSelect(req, res);
    mockService.restore();
});
test('fetchGridData controller test', async () => {
    const res = node_mocks_http_1.default.createResponse();
    const response = new info_query_1.QueryResponse('2019-07-16T14:16:06.314Z', 14, 'completed', 'ad-hoc', 1, 14, null);
    const mockService = sinon_1.default.mock(services_1.outBoundService);
    mockService.expects('fetchGridData').returns(response);
    await __1.outBoundController.fetchGridData(req, res);
    sinon_1.default.assert.match(JSON.parse(res._getData()).status, 'completed');
    mockService.restore();
});
test('executeMultiSelect controller test', async () => {
    const res = node_mocks_http_1.default.createResponse();
    const response = new info_query_1.QueryResponse('2019-07-16T14:16:06.314Z', 14, 'completed', 'ad-hoc', 1, 14, null);
    const mockService = sinon_1.default.mock(services_1.outBoundService);
    mockService.expects('executeMultiSelectQuery').returns(response);
    await __1.outBoundController.executeMultiSelectQuery(req, res);
    sinon_1.default.assert.match(JSON.parse(res._getData()).status, 'completed');
    mockService.restore();
});
//# sourceMappingURL=outbound.controller.test.js.map