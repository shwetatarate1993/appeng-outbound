import { Request, Response } from 'express';
declare class OutBoundController {
    singleSelect(request: Request, response: Response): Promise<void>;
    fetchGridData(request: Request, response: Response): Promise<void>;
    fetchFormDataByGridId(request: Request, response: Response): Promise<void>;
    fetchGridDataByGridId(request: Request, response: Response): Promise<void>;
    fetchSelectOptionData(request: Request, response: Response): Promise<void>;
    executeMultiSelectQuery(request: Request, response: Response): Promise<void>;
}
declare const _default: OutBoundController;
export default _default;
//# sourceMappingURL=outbound.controller.d.ts.map