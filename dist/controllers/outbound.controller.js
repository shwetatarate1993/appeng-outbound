"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const services_1 = require("../services");
class OutBoundController {
    async singleSelect(request, response) {
        const serviceResponse = await services_1.outBoundService.singleSelect(request.params.logicalEntityName, request.params.primaryKeyValue, request.get(info_commons_1.TOKEN_KEY), request.get(info_commons_1.USER_DETAILS_KEY), request.get(info_commons_1.PROVIDER));
        if (serviceResponse instanceof info_commons_1.ErrorResponse) {
            response.status(serviceResponse.code).json(serviceResponse);
        }
        else {
            response.json(serviceResponse);
        }
    }
    async fetchGridData(request, response) {
        const token = request.get(info_commons_1.TOKEN_KEY);
        const serviceResponse = await services_1.outBoundService.fetchGridData(request.params.logicalEntityName, request.body, token, request.get(info_commons_1.USER_DETAILS_KEY), request.get(info_commons_1.PROVIDER));
        if (serviceResponse instanceof info_commons_1.ErrorResponse) {
            response.status(serviceResponse.code).json(serviceResponse);
        }
        else {
            response.json(serviceResponse);
        }
    }
    async fetchFormDataByGridId(request, response) {
        const serviceResponse = await services_1.outBoundService.fetchFormDataByGridId(request.params.datagridId, request.params.primaryKeyValue, request.get(info_commons_1.TOKEN_KEY), request.get(info_commons_1.USER_DETAILS_KEY), request.get(info_commons_1.PROVIDER));
        if (serviceResponse instanceof info_commons_1.ErrorResponse) {
            response.status(serviceResponse.code).json(serviceResponse);
        }
        else {
            response.json(serviceResponse);
        }
    }
    async fetchGridDataByGridId(request, response) {
        const token = request.get(info_commons_1.TOKEN_KEY);
        const serviceResponse = await services_1.outBoundService.fetchGridDataByGridId(request.params.datagridId, request.body, token, request.get(info_commons_1.USER_DETAILS_KEY), request.get(info_commons_1.PROVIDER));
        if (serviceResponse instanceof info_commons_1.ErrorResponse) {
            response.status(serviceResponse.code).json(serviceResponse);
        }
        else {
            response.json(serviceResponse);
        }
    }
    async fetchSelectOptionData(request, response) {
        const token = request.get(info_commons_1.TOKEN_KEY);
        const serviceResponse = await services_1.outBoundService.fetchSelectOptionData(request.params.formFieldName, request.body, token, request.get(info_commons_1.USER_DETAILS_KEY), request.get(info_commons_1.PROVIDER));
        if (serviceResponse instanceof info_commons_1.ErrorResponse) {
            response.status(serviceResponse.code).json(serviceResponse);
        }
        else {
            response.json(serviceResponse);
        }
    }
    async executeMultiSelectQuery(request, response) {
        const token = request.get(info_commons_1.TOKEN_KEY);
        const serviceResponse = await services_1.outBoundService.executeMultiSelectQuery(request.params.logicalEntityName, request.body, token, request.get(info_commons_1.USER_DETAILS_KEY), request.get(info_commons_1.PROVIDER));
        if (serviceResponse instanceof info_commons_1.ErrorResponse) {
            response.status(serviceResponse.code).json(serviceResponse);
        }
        else {
            response.json(serviceResponse);
        }
    }
}
exports.default = new OutBoundController();
//# sourceMappingURL=outbound.controller.js.map