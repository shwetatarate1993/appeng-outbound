"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_query_1 = require("info-query");
class AppengOutboundConfig {
    static configure(config, configuredDialect) {
        console.log("Electron AppengOutboundConfig");
        info_query_1.InfoQueryConfig.configure(config, configuredDialect);
    }
}
exports.default = AppengOutboundConfig;
//# sourceMappingURL=appeng.outbound.config.js.map