"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const config_1 = __importDefault(require("./config"));
const init_config_1 = require("./init-config");
init_config_1.AppengOutboundConfig.configure(config_1.default.get('db'), info_commons_1.DIALECT);
var services_1 = require("./services");
exports.outBoundService = services_1.outBoundService;
exports.OutBoundService = services_1.OutBoundService;
//# sourceMappingURL=app.js.map